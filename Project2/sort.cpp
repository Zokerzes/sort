#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

const long size_ = 100;
int arr[size_];
int counter = 0;
void init_arrey(int mas[size_],long size_) {
	
	for (int  i = 0; i < size_; i++){
		mas[i] = rand() % 100;
	}
}

void print_arrey(int mas[size_], long size_) {
	for (int i = 0; i < size_; i++) {
		cout << mas[i] << "\t";
	}
	cout << "\n\n";
}

template <class T>
void selectSort(T a[], long size) {
	long i, j, k;
	T x;
	// i - ����� �������� ����
	for (i = 0; i < size; i++) {
		k = i;
		x = a[i];
		// ���� ������ ����������� �������� 
		for (j = i + 1; j < size; j++)
		{
			if (a[j] < x) {
				k = j;
				x = a[j];
				// k - ������ ����������� ��������
			}
			counter++;
		}
		if (k != i) {
			a[k] = a[i];
			a[i] = x;
			// ������ ������� ���������� � a[i]
		}
	
	}
}
template <class N>
void bubbleSort(N a[], long size) {
	long i, j;
	N x;
	for (i = 0; i < size-i; i++) { // i - ����� ������� 
		for (j = size - 1; j > i; j--) { // ���������� ����
		// �������
			if (a[j - 1] > a[j]) {
				x = a[j - 1];
				a[j - 1] = a[j];
				a[j] = x;
			}
			counter++;
		}
	}
}

template <class M>
void insertSort(M a[], long size) {
	M x;
	long i, j;
	// ���� ��������, i - ����� ������� 
	for (i = 0; i < size; i++) {
		x = a[i];
		// ����� ����� �������� � �������
		// ������������������
		for (j = i - 1; j >= 0 && a[j] > x; j--)
			// �������� ������� �������,���� �� �����
		{
			a[j + 1] = a[j];
			counter++;
		}
		// ����� �������, �������� �������
		a[j + 1] = x;
	}
}

template <class B>
void shakerSort(B a[], long size) {
	long j, k = size - 1;
	long lb = 1, ub = size - 1; // ������� �����������������
	// ����� �������
	B x;
	do {
		// ������ ����� ����� 
		for (j = ub; j > 0; j--) {
			if (a[j - 1] > a[j]) {
				x = a[j - 1];
				a[j - 1] = a[j];
				a[j] = x;
				k = j;
			}
			counter++;
		}
		lb = k + 1;
		// ������ ������ ���� 
		for (j = 1; j <= ub; j++) {
			if (a[j - 1] > a[j]) {
				x = a[j - 1];
				a[j - 1] = a[j];
				a[j] = x;
				k = j;
			}
			counter++;
		}
		ub = k - 1;
	} while (lb < ub);
}




int main() {
	
	srand(time(NULL));
	init_arrey(arr, size_);
	print_arrey(arr, size_);
	selectSort(arr, size_);
	print_arrey(arr, size_);
	cout << "counter select = " << counter << endl;
	counter = 0;
	init_arrey(arr, size_);
	print_arrey(arr, size_);
	bubbleSort(arr, size_);
	print_arrey(arr, size_);
	cout << "counter bubble = " << counter << endl;
	counter = 0;
	init_arrey(arr, size_);
	print_arrey(arr, size_);
	insertSort(arr, size_);
	print_arrey(arr, size_);
	cout << "counter insert = " << counter << endl;
	counter = 0;
	init_arrey(arr, size_);
	print_arrey(arr, size_);
	shakerSort(arr, size_);
	print_arrey(arr, size_);
	cout << "counter shaker = " << counter << endl;
	return 0;
}